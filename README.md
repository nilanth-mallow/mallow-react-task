# Mallow React Tasks

### Set Up Commands

### `Clone the repo`

### `yarn install`

### `yarn start`


### Login Credential

Username: `eve.holt@reqres.in`
Password: `cityslicka`

### Mock API docs

[REQ | RES](https://reqres.in/)

### React Test Cases

1. Need to show logged username and email in the header
2. Remove the user from the table, If deleted and get a confirmation from the user in a modal before deleting.
3. ReUse CreateUpdateDrawer component to Edit user details.
4. Add a button loader to create the user and close the user create drawer on API success. (Follow this for Edit user)
5. Implement client-side search for users table.
6. Restrict user logout on app refresh.
7. Implement logout
8. Show user details in `<UserDetails/>`, Navigate to detail component on clicking the email in the user's table. (Use single User API).
9. Component needs to follow EsLint rules.
10. Show the created user in the first row of the user's table on API success.
11. Make sure that actions have a good UX.
12. Add card view with infinity scroll, Edit and Delete.
13. Profile image in the user's table needs to be aligned.
14. Show the edit and delete option only on the table row hover.
