import actions from 'redux/Users/actions';
import {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {Button, PageHeader, Input, message, Radio} from 'antd';
import UserTable from 'components/UserTable';
import CreateUpdateDrawer from 'components/CreateUpdateDrawer';

const { Search } = Input;


function Users() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({
      type: actions.GET_USERS,
      payload: { currentPage: 1 },
    });
  },[dispatch]);

  function openCreateUserDrawer(){
    dispatch({
      type: actions.SET_CREATE_USER_DRAWER_VISIBLE,
      payload: true,
    });
  }

  function onSearch(){
    // Add search logic
    message.info('Implement search logic');
  }

  function handleViewType(){
    // Add search logic
    message.info('Implement Card View');
  }

    
  return (
    <>
      <PageHeader
        ghost={false}
        onBack={false}
        title="Users"
        extra={[
          <Search
            key="1"
            placeholder="input search text"
            allowClear
            style={{width: '200px'}}
            onSearch={onSearch}
          />,
          <Button key="2" type="primary" onClick={openCreateUserDrawer}>Create User</Button>,
        ]}
        footer={
          <Radio.Group value="table" size="small" onChange={handleViewType}>
            <Radio.Button value="table">Table</Radio.Button>
            <Radio.Button value="card">Card</Radio.Button>
          </Radio.Group>
        }
      />
      <UserTable/>
      <CreateUpdateDrawer/>
    </>
  );
}

export default Users;
