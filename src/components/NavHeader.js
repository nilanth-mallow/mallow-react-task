import {Button, Layout, message, Space} from 'antd';
import {LogoutOutlined} from '@ant-design/icons';

const { Header } = Layout;

export default function NavHeader(){

  function logOut(){
    // Add logout logic
    message.info('Logout the user');
  }

  return(
    <Header>
      <div className="header-title">
        <Space size="large">
          {/*Display current logged user name and email*/}
          <div>Elon Musk</div>
          <Button type="primary" icon={<LogoutOutlined />} danger onClick={logOut}/>
        </Space>
      </div>
    </Header>
  );
}
