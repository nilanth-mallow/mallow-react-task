import React from 'react';
import {Drawer, Button, Form, Input} from 'antd';
import {useDispatch, useSelector} from 'react-redux';
import actions from 'redux/Users/actions';

export default function CreateUpdateDrawer() {
  const dispatch = useDispatch();
  const {createUserDrawerVisible} = useSelector(state => state.usersReducer);
  const [form] = Form.useForm();

  const onClose = () => {
    dispatch({
      type: actions.SET_CREATE_USER_DRAWER_VISIBLE,
      payload: false,
    });
  };

  const onSubmit = () => {
    form
      .validateFields()
      .then((values) => {
        form.resetFields();
        dispatch({
          type: actions.CREATE_USER,
          payload: values,
        });
      })
  };

  return (
    <Drawer
      title="Create New User"
      width={400}
      onClose={onClose}
      visible={createUserDrawerVisible}
      destroyOnClose={true}
      footer={
        <div
          style={{
            textAlign: 'right',
          }}
        >
          <Button onClick={onClose} style={{marginRight: 8}}>Cancel</Button>
          <Button onClick={onSubmit} type="primary">Submit</Button>
        </div>
      }
    >
      <Form form={form} layout="vertical" >
        <Form.Item
          name="first_name"
          label="First Name"
          rules={[{required: true, message: 'Please enter first name'}]}
        >
          <Input placeholder="Please enter first name"/>
        </Form.Item>
        <Form.Item
          name="last_name"
          label="Last Name"
          rules={[{required: true, message: 'Please enter last name'}]}
        >
          <Input placeholder="Please enter last name"/>
        </Form.Item>
        <Form.Item
          name="email"
          label="Email"
          rules={[{required: true, message: 'Please enter email'}]}
        >
          <Input placeholder="Please enter email"/>
        </Form.Item>
        <Form.Item
          name="avatar"
          label="Profile Image Link"
          rules={[{required: true, message: 'Please enter profile image link'}]}
        >
          <Input placeholder="Please enter profile image link"/>
        </Form.Item>
      </Form>
    </Drawer>
  );
};
