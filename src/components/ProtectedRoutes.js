// Here we include the components which need to be accesses after successful login.
import {Route, Switch} from 'react-router-dom';
import Users from 'components/Users';
import UserDetails from 'components/UserDetails';
import {Layout} from 'antd';
import NavHeader from 'components/NavHeader';

const {Content} = Layout;

export default function ProtectedRoutes() {
  return (
    <Layout className="layout">
      <NavHeader/>
      <Content style={{padding: '50px'}}>
        <Switch>
          <Route path="/users">
            <Users/>
          </Route>
          <Route path="/user/id">
            <UserDetails/>
          </Route>
        </Switch>
      </Content>
    </Layout>
  );
}
