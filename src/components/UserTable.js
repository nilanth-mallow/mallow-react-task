import {Avatar, Button, message, Space, Table} from 'antd';
import {useDispatch, useSelector} from 'react-redux';
import actions from 'redux/Users/actions';
import {NavLink} from 'react-router-dom';

export default function UserTable() {
  const dispatch = useDispatch();
  const {userData, userTableLoader, totalUsers} = useSelector(state => state.usersReducer)
    
  function editUser(){
    dispatch({
      type: actions.SET_CREATE_USER_DRAWER_VISIBLE,
      payload: true,
    });
  }

  function deleteUser(userID){
    //Add delete action
    message.info('Implement delete logic');
  }
    
  const columns = [
    {
      title: '',
      dataIndex: 'avatar',
      width: '20%',
      render: (avatar) => <Avatar size="large" src={avatar}/>,
    },
    {
      title: 'Email',
      dataIndex: 'email',
      width: '20%',
      render: (email, rowData) => <NavLink to="{rowData.id}">{email}</NavLink>,
    },
    {
      title: 'First Name',
      dataIndex: 'first_name',
      width: '20%',
    },
    {
      title: 'Last Name',
      dataIndex: 'last_name',
      width: '20%',
    },
    {
      title: 'Action',
      key: '',
      render: (data) => (
        <Space size="middle">
          <Button type="primary" onClick={editUser}>Edit</Button>
          <Button type="primary" danger onClick={() => deleteUser(data.id)}>Delete</Button>
        </Space>
      ),
    },
  ];

  // const handleTableChange = (pagination, filters, sorter) => {
  //   dispatch({
  //     type: actions.GET_USERS,
  //     payload: { currentPage: pagination.current },
  //   });
  // };

  return (
    <Table
      columns={columns}
      rowKey={record => record.id}
      dataSource={userData}
      loading={userTableLoader}
      // onChange={handleTableChange}
      scroll={{y: 800}}
      pagination={{pageSize: 5, total: totalUsers}}
    />
  );
}

