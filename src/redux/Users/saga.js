import {all, call, put, takeLatest} from 'redux-saga/effects'
import actions from 'redux/Users/actions';
import {deleteRequest, getRequest, postRequest} from 'Config/axiosClient'


function* getUsers(action) {
  try {
    const response = yield call(() => getRequest(`users?page=${action.payload.currentPage}&per_page=12`));
    yield put({type: actions.GET_USERS_SUCCESS, response});
  } catch (e) {
    yield put({type: actions.GET_USERS_FAILURE});
  }
}

function* createUser(action) {
  try {
    const response = yield call(() => postRequest('users', action.payload));
    yield put({type: actions.CREATE_USER_SUCCESS, response});
  } catch (e) {
    yield put({type: actions.CREATE_USER_FAILURE});
  }
}

function* deleteUser(action) {
  try {
    yield call(() => deleteRequest(`users/${action.payload}`));
    // yield put({type: actions.DELETE_USER_SUCCESS, response});
  } catch (e) {
    // yield put({type: actions.DELETE_USER_FAILURE});
  }
}


export default function* rootSaga() {
  yield all([takeLatest(actions.GET_USERS, getUsers)]);
  yield all([takeLatest(actions.CREATE_USER, createUser)]);
  yield all([takeLatest(actions.DELETE_USER, deleteUser)]);
}
