import actions from 'redux/Users/actions';

const initialState = {
  userData: [],
  userTableLoader: false,
  createUserDrawerVisible: false,
  totalUsers: 0,
}

function Reducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_USERS:
      return {...state, userTableLoader: true}
    case actions.GET_USERS_SUCCESS:
      return {
        ...state,
        userData: action.response.data.data,
        totalUsers: action.response.data.total,
        userTableLoader: false,
      }
    case actions.GET_USERS_FAILURE:
      return {...state, userData: [], userTableLoader: false}
    case actions.SET_CREATE_USER_DRAWER_VISIBLE:
      return {...state, createUserDrawerVisible: action.payload}
    case actions.CREATE_USER:
      return {...state}
    case actions.CREATE_USER_SUCCESS:
      return {...state, userData: []}
    case actions.CREATE_USER_FAILURE:
      return {...state}
    case actions.DELETE_USER:
      return {...state}
    default:
      return state
  }
}

export default Reducer;
